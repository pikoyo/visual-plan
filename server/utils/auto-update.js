let chokidar = require('chokidar')
let path = require('path')
let getFileList = require('./get-file-list')
let debounce = require('./debounce')

function clearCache (dir) {
  let list = getFileList(dir)
  for (let path of list) { delete require.cache[require.resolve(path)] }
  return list
}

function hotUpdate (dir, handler = () => null) {
  // 处理函数
  let onChange = function (event, path) {
    clearCache(dir)
    if (handler instanceof Function) {
      handler(path)
    }
  }
  // 实现热更新
  chokidar.watch('./').on('change', debounce(onChange))
  if (handler instanceof Function) {
    handler(path)
  }
}

module.exports = hotUpdate
