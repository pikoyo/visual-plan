import request from '../utils/request'

// to prevent network request
let loadCache = []

export async function getPlanList () {
  // return request.get('/plan')
  console.log(process.env.PLANS)
  return {
    data: process.env.PLANS
  }
}

export function getPlan (i) {
  // use cache
  if (!loadCache[i]) {
    loadCache[i] = request.get(process.env.PLANS[i].path)
  }
  return loadCache[i]
}

// args可以添加搜索参数
export async function getCourses (id, query) {
  let { data } = await getPlan(id)
  console.log(query)
  query = Object.assign({
    page: 1,
    numsPerPage: 20,
    term: -1,
    type: -1,
    direction: null,
    search: null,
    optional: null
  }, query)
  let results = Object.values(data.courses) || []
  let directions = data.directions
  let start = query.numsPerPage * (query.page - 1)
  let end = query.numsPerPage * (query.page)
  results = results.filter(val => {
    let fit = true
    if (Number(query.term) !== -1 && Number(val.term) !== Number(query.term)) fit = false
    if (Number(query.type) !== -1 && Number(val.type) !== Number(query.type)) fit = false
    if (query.optional != null && query.optional !== val.optional) fit = false
    if (query.direction != null && val.direction !== -1 && directions[query.direction] && directions[query.direction].indexOf(val.direction) === -1) fit = false
    if (query.search && val.name.indexOf(query.search) === -1 && val.id.indexOf(query.search) === -1 && val.description.indexOf(query.search) === -1) fit = false
    return fit
  }).map(val => {
    // 添加 flags
    val.tags = /* taggedCourses[val.id] || */[]
    if (val.name.indexOf('实验') !== -1 && val.tags.indexOf('实验') === -1) val.tags.push('实验')
    return val
  })
  return {
    data: {
      length: results.length,
      results: results.slice(start, end)
    }
  }
  /* return request.get('/plan/' + i + '/courses', {
    params: {
      page: page,
      numsPerPage: 20,
      ...args
    }
  }) */
}

export async function getCourseTypes (id) {
  let plan = await getPlan(id)
  return {
    data: {
      length: plan.data.courses_type.length,
      results: plan.data.courses_type
    }
  }
  // return request.get('/plan/' + i + '/course_types')
}

export async function getDirections (id) {
  let plan = await getPlan(id)
  return {
    data: {
      length: plan.data.directions.length,
      results: plan.data.directions
    }
  }
  // return request.get('/plan/' + i + '/directions')
}
