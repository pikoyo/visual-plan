const chalk = require('chalk')

module.exports = {
  error (...msg) {
    console.log(chalk.bgRen.white(' E ') + ` `, ...msg)
    console.log(' ')
  },
  warning (...msg) {
    console.log(chalk.bgOrange.white(' W ') + ` `, ...msg)
    console.log(' ')
  },
  info (...msg) {
    console.log(chalk.bgBlue.white(' I ') + ` `, ...msg)
    console.log(' ')
  },
  success (...msg) {
    console.log(chalk.bgGreen.white(' S ') + ` `, ...msg)
    console.log(' ')
  }
}
