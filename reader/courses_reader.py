import re
import pandas as pd

# 依据原始表格数据创建df


def create_raw_df(data):
    df = pd.DataFrame(data, columns=['name', 'id', 'credit', 'total_time',
                                     'teach_time', 'practice_time', 'exp_time', 'term', 'description'])
    return df


def get_courses_table_data(node):
    # 原始的课程数据
    courses_df = create_raw_df([])
    for page in node['pages']:
        tables = page.extract_tables()
        tables_df = create_raw_df([])
        for table in tables:
            # 课表有8或9列，但是识别错误可能导致产生10+列的表格
            table_raw = [row for row in table if len(
                row) >= 8 and row[0] != None and len(row[0].splitlines(False)) > 1]
            table_df = pd.DataFrame(table_raw)

            # 清理无效列
            if len(table_df.columns) > 9:
                for i in table_df.columns:
                    if table_df[i].isnull().all():
                        table_df = table_df.drop(i, axis=1)

            # 仍然有多余列，舍掉
            if len(table_df.columns) == 9:
                table_df.columns = ['name', 'id', 'credit', 'total_time',
                                    'teach_time', 'practice_time', 'exp_time', 'term', 'description']
                tables_df = pd.merge(tables_df, table_df, how="outer")
            elif len(table_df.columns) == 8:
                table_df.columns = ['name', 'id', 'credit', 'total_time',
                                    'teach_time', 'practice_time', 'exp_time', 'term']
                table_df['description'] = ''
                tables_df = pd.merge(tables_df, table_df, how="outer")

        courses_df = pd.merge(courses_df, tables_df, how="outer")

    # 清理换行符
    courses_df['name'] = courses_df['name'].str.replace('\n', '')

    # 清理因换页导致表格数据分页的问题
    break_lines = []
    for i in range(len(courses_df)):
        if courses_df.loc[i]['credit'] == '':
            courses_df.loc[i-1]['name'] = courses_df.loc[i -
                                                         1]['name'] + courses_df.loc[i]['name']
            break_lines.append(i)
    courses_df = courses_df.drop(labels=break_lines)
    courses_df['direction'] = -1
    courses_df['type'] = -1

    # 判断必选修
    # if re.search('选修', node['text']):
    #    courses_df['optional'] = True
    # else:
    #    courses_df['optional'] = False

    print('%s:已获取 %d 门课' % (node['text'], len(courses_df)))
    return courses_df

# 按照必修选修来获取课程


def get_courses_data_by_optional(parent_node):
    children = parent_node['children']
    courses_df = create_raw_df([])
    # 没有子类别，只有必修/选修
    if len(children) == 0:
        courses_df = get_courses_table_data(parent_node)
    for i in range(len(children)):
        current_courses = get_courses_table_data(children[i])
        current_courses['optional'] = bool(i)
        courses_df = pd.merge(courses_df, current_courses, how="outer")
    return courses_df


# 获取专业方向

def get_directions(parent_node):
    directions = []
    children = parent_node['children']
    courses_df = create_raw_df([])
    for i in range(len(children)):
        # 添加专业方向
        direction_name = re.search(
            '[^\d（）.]+(?=方向)', children[i]['text']).group()
        directions.append(direction_name)
    return directions


# 通过专业方向获取课程
def get_courses_data_by_direction(parent_node):
    children = parent_node['children']
    courses_df = create_raw_df([])
    for i in range(len(children)):
        current_courses = get_courses_data_by_optional(children[i])
        current_courses['direction'] = i
        courses_df = pd.merge(courses_df, current_courses, how="outer")
    return courses_df


def read_courses(doc_tree):

    # 课程数据
    raw_courses = doc_tree['children'][6]['children']
    courses_df = pd.DataFrame([], columns=[
                              'id', 'name', 'type', 'direction', 'optional', 'credit', 'total_time', 'teach_time', 'practice_time', 'exp_time', 'term', 'description'])
    for i in range(len(raw_courses)):
        typed_course = raw_courses[i]
        current_courses = create_raw_df([])
        print('(%d,%d) 加载 %s，子类别 %d' % (i + 1, len(raw_courses),
                                        typed_course['text'], len(typed_course['children'])))
        # 必修/选修课
        if not re.search('方向', typed_course['text']):
            # 必修和选修课
            current_courses = get_courses_data_by_optional(typed_course)
            current_courses['direction'] = -1
        else:
            current_courses = get_courses_data_by_direction(typed_course)
        current_courses['type'] = i
        courses_df = pd.merge(courses_df, current_courses, how="outer")

    courses_df = courses_df.loc[courses_df.name != '课程名称（中英文）']
    courses_df['english_name'] = courses_df['name'].str.replace(
        '[\u4e00-\u9fa50-9A-Z（）]+\s', '')
    courses_df['name'] = courses_df['name'].str.replace('\s[a-zA-Z].+', '')


def walk_courses(course_start_node, direction=None, optional=None):
    """
    根据文档树遍历专业方向和课程
    """
    courses_df = pd.DataFrame([], columns=[
        'id', 'name', 'platform', 'direction', 'optional', 'credit', 'total_time', 'teach_time', 'practice_time', 'exp_time', 'term', 'description'])

    directions = []

    children_node = course_start_node["children"]

    for node in children_node:
        direction = direction if direction is not None else re.search(
            '[^\d（）.]+(?=方向)', node['text'])
        optional = optional if optional is not None else bool(
            re.search('选修', node['text']))
        if direction:
            directions.append(direction)

        if len(node['children']) > 0:
            node_directions, node_courses_df = walk_courses(
                node, direction, optional)
            directions.extend(node_directions)
            pd.merge(courses_df, node_courses_df, how="outer")
        else:
            node_courses_df = get_courses_table_data(node)
            node_courses_df['optional'] = optional
            node_courses_df['direction'] = direction
            pd.merge(courses_df, node_courses_df, how="outer")

    return directions, courses_df
