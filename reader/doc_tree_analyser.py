import re
# 匹配文档层次


def match_index(str):
    index_regex = [".*专业.*培养方案[^：]*$", "^[一二三四五六七八九十]+、.+$", "^（[一二三四五六七八九十]+）.+$",
                   "(^[0-9]+\.([^0-9：。.]+((（.*）.*)*)))|(通识教育课程平台必修课程)", "^（[0-9]+）.+$"]
    for i in range(len(index_regex)):
        if re.search(index_regex[i], str):
            return i
    return -1

# 输出节点


def output_node(node):
    blank = ""
    for depth in range(node['depth']):
        blank += "----"
    print(blank + node['text'])

# 页面裁剪


def crop_page(page, top, bottom):
    crop_box = (0, top, page.width, bottom)
    if top >= bottom:
        return None
    return page.crop(crop_box)

# 创建节点


def create_node(top, bottom, depth, text="", parent=None):
    return {
        "parent": parent,
        "depth": depth,
        "top": top,
        "bottom": bottom,
        "pages": [],
        "text": text,
        "children": []
    }

# 分析PDF文档，构建文档树


def analyse(pdf):
    # 文档分级解析
    root = create_node(0, 0, 0, "ROOT")
    current_depth = 0
    parent = root
    last = root
    for page in pdf.pages:
        word_list = page.extract_words()
        has_matched = False
        bottom = 0
        for word in word_list:
            depth = match_index(word['text'])
            # 判断是否为标题
            if depth == -1:
                continue

            if depth == 0:
                root['text'] = word['text']
                root['top'] = word['top']
                root['bottom'] = word['bottom']
                output_node(root)
                has_matched = True
                continue
            # 裁剪页面
            croped = crop_page(page, bottom, word['top'] - 1)
            if croped != None:
                last['pages'].append(croped)

            # 比较深度，决定父节点
            if depth > current_depth:
                parent = last
            elif depth < current_depth:
                for delta in range(abs(depth - current_depth)):
                    if parent['parent'] != None:
                        parent = parent['parent']

            # 添加节点
            node = create_node(word['top'], word['bottom'],
                               depth, word['text'], parent)
            parent['children'].append(node)

            # 标志修改
            last = node
            has_matched = True
            # output_node(node)
            current_depth = depth
            bottom = last['bottom'] + 1

        # 整个页面没有匹配到标题
        if not has_matched:
            last['pages'].append(page)
        else:
            # 该页面的最后一个标题
            croped = crop_page(page, last['bottom'] + 2, page.height)
            if croped != None:
                last['pages'].append(croped)

    return root
