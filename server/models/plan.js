let taggedCourses = require('../tagged-courses.json')
let actions = {
  // 列表
  getList () {
    let data = require('../data')
    return data.map((val, id) => {
      return {
        id: id,
        name: val.name
      }
    })
  },
  // 基本信息
  getOne (id) {
    let data = require('../data')
    let result = data[id]
    if (!result) { return null }
    return {
      id: id,
      name: result.name,
      description: result.description,
      core_courses: result.core_courses,
      directions: result.directions,
      degree: result.degree,
      credit: result.credit,
      length: result.length
    }
  },
  // 课程
  getCourses (id, query = {}) {
    let data = require('../data')
    query = Object.assign({
      page: 1,
      numsPerPage: 20,
      term: -1,
      type: -1,
      direction: null,
      search: null,
      optional: null
    }, query)
    let results = Object.values(data[id].courses) || []
    let directions = data[id].directions
    let start = query.numsPerPage * (query.page - 1)
    let end = query.numsPerPage * (query.page)
    results = results.filter(val => {
      let fit = true
      if (Number(query.term) !== -1 && Number(val.term) !== Number(query.term)) fit = false
      if (Number(query.type) !== -1 && Number(val.type) !== Number(query.type)) fit = false
      if (query.optional != null && query.optional !== String(val.optional)) fit = false
      if (query.direction != null && val.direction !== -1 && directions[query.direction] && directions[query.direction].indexOf(val.direction) === -1) fit = false
      if (query.search && val.name.indexOf(query.search) === -1 && val.id.indexOf(query.search) === -1 && val.description.indexOf(query.search) === -1) fit = false
      return fit
    }).map(val => {
      // 添加 flags
      val.tags = taggedCourses[val.id] || []
      if (val.name.indexOf('实验') !== -1 && val.tags.indexOf('实验') === -1) val.tags.push('实验')
      return val
    })
    return {
      length: results.length,
      results: results.slice(start, end)
    }
  },
  // 专业方向
  getDirections (id) {
    let data = require('../data')
    return {
      length: data[id].directions.length,
      results: data[id].directions
    }
  },
  getCourseTypes (id) {
    let data = require('../data')
    return {
      length: data[id].courses_type.length,
      results: data[id].courses_type
    }
  }
}

module.exports = actions
