
// 生成一个函数的防抖 (debounce) 版本
module.exports = function (func) {
  let timer = null
  return function (...args) {
    if (timer !== null) clearTimeout(timer)
    timer = setTimeout(func, 500, args)
  }
}
