/**
 * 加载API路由
 * @param {Express} app Express Instance / webpack-dev-sever instance
 */
module.exports = function (app) {
  /* 注册路由 */
  for (let route of require('../routes')) {
    app[route.method.toLowerCase()](`/api${route.path}`, route.handler.bind(this))
  }
}
