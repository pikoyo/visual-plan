'use strict';
let fs = require('fs');
let path = require('path');
let planPath = 'static/data/plans/'

let plans = fs.readdirSync(path.join(__dirname, '../', planPath))

plans = plans.map((filename, index) => {
  let ctx = require(path.join('../', planPath,filename))
  let localPath = path.join(path.join('./', planPath), filename)
  return {
    id: index,
    name: ctx.name,
    path: localPath
  }
})

plans = JSON.stringify(plans)

module.exports = {
  NODE_ENV: '"production"',
  PLANS: `${plans}`,
  PLANS_PATH: `"${planPath}"`
}
