# 培养方案可视化

****

**此项目正在重构成无服务端项目。可能会出现大规模代码变更。**

****

通过 `Python` 将 `PDF` 转换成 `json` 结构，通过 `js`和 `vue` 进行可视化。

> 由于格式原因，只支持湖大2018版方案

## 技术栈

前端(js)：`Vue`全家桶、`element-ui`

服务端(js)： `express`

数据处理(python)：`pdfplumber`、`pandas`

## 安装使用

``` bash
# 安装依赖
npm install

# 运行开发服务器 at localhost:8080
npm run dev

# 构建前端文件
npm run build

```

## 文件结构
+	`build` ：WebPack 配置
+	`config`：环境配置
+	`dist`：打包后的目录
+	`server`：服务端目录(即将废弃)
+	`reader`：培养方案读取
+	`client`：客户端目录
+	`static`：静态资源目录

## 已知BUG
1.	偶尔会造成`js`引擎堆溢出
2.	`PDF` 部分数据读取存在问题