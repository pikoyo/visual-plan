/**
 * 培养方案-模型
 */
let fs = require('fs')
let path = require('path')
const autoUpdate = require('./utils/auto-update')

let dir = path.resolve(__dirname, '../data/json')

// 加载文件
let loadFiles = function () {
  let planList = fs.readdirSync(dir)
  let plansData = []

  for (let plan of planList) {
    plansData.push(require(dir + '/' + plan))
  }
  return plansData
}
module.exports = loadFiles()
/* autoUpdate(dir, function () {
  delete require.cache[require.resolve('./data')]
  module.exports = loadFiles()
}) */
