const express = require('express')
const autoUpdate = require('./utils/auto-update')
const httpLog = require('./utils/http-log')
const log = require('./utils/log')
const app = express()
const port = 2020

// static server
app.use(express.static('./dist'))

app.use(httpLog)

// api server with hot-update
autoUpdate(__dirname, function () {
  delete app.routes
  require('./utils/create-routes')(app)
  log.info('服务器已更新')
})

require('./utils/create-routes')(app)

app.listen(port, () => log.info(`服务器地址:http://127.0.0.1:${port}/`))
