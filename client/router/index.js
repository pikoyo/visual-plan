import Vue from 'vue'
import Router from 'vue-router'
import store from '../store/index'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      // redirect: `/${store.state.page || 0}/index`
      beforeEnter (to, from, next) {
        next({
          name: 'index',
          params: {
            id: store.state.plan || 0
          },
          meta: {
            title: '首页'
          }
        })
      }
    },
    {
      path: '/:id/index',
      name: 'index',
      component: () => import('@/pages/index'),
      meta: {
        title: '概述'
      }
    }, {
      path: '/:id/courses/list',
      name: 'courses',
      component: () => import('@/pages/courses'),
      meta: {
        title: '课程列表'
      }
    }, {
      path: '/:id/courses/selection',
      name: 'courses-selection',
      component: () => import('@/pages/selection/index'),
      meta: {
        title: '选课方案'
      }
    },
    {
      path: '/:id/courses/selection/:sid',
      name: 'selection-detail',
      component: () => import('@/pages/selection/detail'),
      title: '选课方案详细'
    }
  ]
})

router.afterEach((to) => {
  // ID 变更自动存储
  if (to.params.id !== null && to.params.id !== undefined) {
    store.commit('changePlan', to.params.id)
  }

  // 设置浏览器标题
  let siteName = '培养方案可视化平台'
  document.title = to.meta.title ? to.meta.title + ' - ' + siteName : siteName
})

export default router
