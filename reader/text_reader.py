def get_text_from_multipage(node, include_title=False):
    text = ""
    if include_title:
        text = node['text']
    for page in node['pages']:
        parsed = page.extract_text()
        if parsed:
            text += parsed + '\n'
    return text.strip()
