import axios from 'axios'
import { Message } from 'element-ui'

let request = axios.create({
  baseURL: './'
})

request.interceptors.response.use(function (response) {
  // 响应数据
  return response
}, function (error) {
  // 响应错误
  Message.error('网络异常')
  return Promise.reject(error)
})

export default request
