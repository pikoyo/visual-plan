let log = require('./log')
module.exports = function (req, res, next) {
  log.info(req.method, ' ', req.originalUrl)
  next()
}
