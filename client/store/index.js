import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

const store = new Vuex.Store({
  state: {
    plan: 0,
    selections: [],
    selections_index: 0
  },
  mutations: {
    changePlan (state, n) {
      state.plan = n
    },
    addSelection (state, data) {
      data.id = state.selections_index
      // id, plan_id, selected_courses
      state.selections.push(data)
      state.selections_index++
    },
    updateSelection (state, { id, data }) {
      let index = state.selections.findIndex(val => val.id === id)
      data.id = index
      // state.selections[index] = data
      Vue.set(state.selections, index, data)
    },
    deleteSelection (state, id) {
      let index = state.selections.findIndex(val => val.id === id)
      state.selections.splice(index, 1)
    }
  },
  getters: {
    selections: (state) => (planId, search) => {
      return state.selections.filter(val => {
        if (search && val.name.indexOf(search) === -1 && val.description.indexOf(search) === -1 && val.direction.indexOf(search) === -1) {
          return false
        }
        return val.planId === planId
      })
    },
    selection: (state) => (id) => {
      return state.selections.find(val => val.id === id)
    }
  },
  plugins: [vuexLocal.plugin]
})

export default store
