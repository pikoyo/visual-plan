let fs = require('fs')

function getFileTrees (dir) {
  var children = []
  fs.readdirSync(dir).forEach(function (filename) {
    let path = dir + '/' + filename
    let stat = fs.statSync(path)
    if (stat && stat.isDirectory()) {
      children = children.concat(getFileTrees(path))
    } else {
      children.push(path)
    }
  })

  return children
}

module.exports = getFileTrees
