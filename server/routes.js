let plan = require('./models/plan')
module.exports = [
  {
    path: '/plan/:id/courses',
    method: 'get',
    handler (req, res) {
      let id = req.params.id
      res.json(plan.getCourses(id, req.query))
    }
  },
  {
    path: '/plan/:id/directions',
    method: 'get',
    handler (req, res) {
      let id = req.params.id
      res.json(plan.getDirections(id))
    }
  },
  {
    path: '/plan/:id/course_types',
    method: 'get',
    handler (req, res) {
      let id = req.params.id
      res.json(plan.getCourseTypes(id))
    }
  },
  {
    path: '/plan/:id',
    method: 'get',
    handler (req, res) {
      let id = req.params.id
      res.json(plan.getOne(id))
    }
  },
  {
    path: '/plan',
    method: 'get',
    handler (req, res) {
      res.json(plan.getList())
    }
  }
]
